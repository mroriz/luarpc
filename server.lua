--
-- Trabalho 1 - Sistemas Distribuídos
-- Biblioteca RPC
-- Spec: http://www.inf.puc-rio.br/~noemi/sd-14/trab1.html
--
-- Student: Marcos Roriz <mroriz@inf.puc-rio.br>
-- Student: Rodrigo Costa <rodrigocosta@telemidia.puc-rio.br>
--
-- File: server.lua
--

-- imports
local luarpc = require ("luarpc")

servant1 = {
  foo = function (a, b, s)
          return a+b, "alo alo"
        end,
  boo = function (n)
          return n
        end,
  bar = function()
          -- print("BAR")
        end,
}

servant2 = {
  foo = function (a, b, s)
          return a - b, "tchau"
        end,
  boo = function (n)
          return 1
        end
}


local ip, p = luarpc.createServant ("interface", servant1)
print ('[First Servant] IP: ' .. ip .. ' Port: ' .. p)

local ip, p = luarpc.createServant ("interface", servant2)
print ('[Second Servant] IP: ' .. ip .. ' Port: ' .. p)

-- arg options
if (arg[1] == "true") then
  luarpc["usingpool"] = true
  luarpc["poolsize"]  = tonumber(arg[2])
else
  luarpc["usingpool"] = false
end

luarpc.waitIncoming()
