#!/bin/bash

for i in `seq 1 $1`;
do
  { time lua client.lua $2 $3 $4; }&
done
