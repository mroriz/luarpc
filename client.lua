--
-- Trabalho 1 - Sistemas Distribu�dos
-- Biblioteca RPC
-- Spec: http://www.inf.puc-rio.br/~noemi/sd-14/trab1.html
--
-- Student: Marcos Roriz <mroriz@inf.puc-rio.br>
-- Student: Rodrigo
--
-- File: client.lua
--

-- imports
local luarpc = require("luarpc")

-- client main routine
local p1 = luarpc.createProxy(arg[1], arg[2], "interface")

for i = 1, arg[3] do
  -- print("---------------- p1.bar() -----------------")
  reply, err = p1.bar()
  -- print(reply, err)

  
  -- print("---------------- p1.foo(1111122222, 123456789) -----------------")
  reply = p1.foo(111112222, 
                 123456789)
  -- print(reply)

  -- print("---------------- p1.foo(20,40) -----------------")
  reply = p1.foo(20, 40)
  -- print(reply)

  -- print("---------------- p1.boo('PUC RIO\\n LAC') ---------------- ")
  reply = p1.boo("PUC-Rio\nLAC\nTelemidia")
  -- print(reply)

  reply = p1.boo([[O protocolo � baseado na troca de strings ascii. Cada chamada � realizada pelo nome do m�todo seguido da lista de par�metros in.
                   Entre o nome do m�todo e o primeiro argumento, assim como depois de cada argumento, deve vir um fim de linha.]])
  -- print("client...")
end


