--
-- Trabalho 1 - Sistemas Distribuídos
-- Biblioteca RPC
-- Spec: http://www.inf.puc-rio.br/~noemi/sd-14/trab1.html
--
-- Student: Marcos Roriz <mroriz@inf.puc-rio.br>
-- Student: Rodrigo Costa <rodrigocosta@telemidia.puc-rio.br>
--
-- File: luarpc.lua
--

-- imports
local io     = require("io")
local socket = require("socket")

-- module
local RPC = {
  poolsize      = 10,
  usingpool     = true
}
     
-- local variables
local idl
local server
local sckts = {}

-- local server variables
local clientcount   = 0
local waitSockets   = {}
local clientSockets = {}
local rpcServants   = {}

-- debug function
function dprint(tbl)
  for k, v in pairs(tbl) do
    print(k, v)
    if (type(v) == "table") then
      dprint(v)
    end
  end
end

-------------------------------------------------------------------------------
-- internal functions
-------------------------------------------------------------------------------

--- This function read and store the client RPC IDL interface.
-- @param tbl A table describing the RPC interface (according to the spec).
function interface(tbl)
  idl = tbl
end

--- An error function that we will use if an error occurs during RPC calls,
-- The function returns the "___ERRORPC" string as specified in the spec and additional information (...) to the client.
-- @param errorMsg An optional string containing additional information of the error ocurred.
-- @return errorFunction A function that prints a string representing an error.
local function errorFunction(errorMsg)
  return "___ERRORPC " .. errorMsg
end

--- Checks if a number is int.
-- This function checks if a number is a integer. This is required, because a number can be a float in lua.
-- @param n A number to be checked.
-- @return true  In case n is a integer number.
--         false Case n is a real number.
local function isInt(n)
  return n == math.floor(n)
end

--- Normalize rpc parameters to the class protocol.
-- @param param A table containing the rpc parameters.
-- @param size A number that represent the number of parameters received.
-- @return nparam A table containig the rpc parameters normalized in the class protocol.
local function normalize(param, size)
  local nparam = {}

  for i = 1, size do
    local p = param[i]
    
    if (p == nil) then
      p = "nil"
    elseif (type(p) == "string") then
      p = string.gsub(p, "\n", "\\n")
      p = "\"" .. p .. "\""
    end
    
    nparam[i] = p
  end
  
  return nparam
end

--- Unnormalize rpc parameters from the class protocol.
-- @param nparam A table containing the normalized rpc parameters (according to the protocol).
-- @return param A table containig the rpc parameters.
local function unnormalize(nparam)
  local param = {}

  for i = 1, #nparam do
    local p = nparam[i]
    
    if (p == "nil") then
      p = nil
    else
      local hasEscape
      p, hasEscape = string.gsub(p, '"', '"')
      
      if (hasEscape == 0) then -- number
        p = tonumber(p)
      else            -- string
        p = string.gsub(p, "\\n", "\n")           -- transform \\n to \n
        p = string.sub(p, 2, string.len(p) - 1)   -- remove '"' from beginning and end of string
      end
    end
    
    nparam[i] = p
  end
  
  return nparam
end

--- This function returns a TCP socket connection to a given hostname and port. 
-- If no socket exists the function create a new TCP socket to the given hostname and port.
-- Clients use this function to reuse a socket to a server, while the server use 
-- the function to return a host to a given client. 
-- If the socket is closed a new one is created.
-- @param rpcHost The hostname of the server/client.
-- @param rpcPort The port of the server/client.
-- @return sck A TCP socket to the given hostname:port.
local function getSocket(rpcHost, rpcPort)
  local key = rpcHost .. ":" .. rpcPort -- rpcHost:rpcPort
  local sck = sckts[key]
  
  if sck == nil then
    sck  = socket.connect(rpcHost, rpcPort)

    if sck then
      sck:setoption("tcp-nodelay", true)
      sckts[key] = sck
    end
  end
  
  return sck
end

--- This function restart a closed TCP connection.
-- @param rpcHost The hostname of the server/client.
-- @param rpcPort The port of the server/client.
-- @return sck A TCP socket to the given hostname:port.
local function renewConnection(rpcHost, rpcPort)
  local key  = rpcHost .. ":" .. rpcPort -- rpcHost:rpcPort
  sckts[key]:close()
  sckts[key] = nil
  return getSocket(rpcHost, rpcPort)
end

-- Check param function
-- FIXME: Should this function be more generic? In a sense that it does not rely on the in/inout description.
--        Ex: typeArray{int, double, float} - params{2, 2, 2.3}   = match
--        Ex: typeArray{int, double, float} - params{2.4, 2, 2.3} = not match
local function checkParameters(typeArray, parameters)
  --local t = {"double", "double", "string"}
  --typeArray -> idl array args
  --parameters -> parameters passed when this function was called
  --correctedTypes -> array with expected parameters type
  local correctedTypes =  {}

  for _, args in pairs(typeArray) do
    if (args.direction == "in") or (args.direction == "inout") then
      correctedTypes[#correctedTypes + 1] = args.type
    end
  end
  local check = true

  --User call a function passing less parameters
  --than expected.
  if (#parameters ~= #correctedTypes) then
    return false
  end

  local index = 1
  for _, i in pairs(parameters) do
    local argType = type (i)
       
    --If 'i' is a number (double or int, for instance), 'type (i)'
    --will return 'number' while the expected type may be
    --a double or int. If type is number and expected type
    --is 'double', so this is a correct situation. If
    --type is number and expected type is 'int', we need to
    --test if 'i' really is 'int'. For this, we create a function
    --'isInt' that retuns 'true' if 'i' is 'int' or 'false' otherwise.
    if (argType ~= 'number') then
      if (argType ~= correctedTypes[index]) then
        check = false
      end
    else --argType == number
      local expectedType = correctedTypes [index]
      if (expectedType == 'int') then
        if not (isInt(i)) then
          check = false
        end
      end
    end

    if (index == #correctedTypes) then
      break
    end
    index = index + 1
  end

  return check
end

--- Generate a client stub for a RPC request.
-- This function generates an anonymous function, stub, that transform the client RPC request
-- into a series of send and receives primitives calls to the RPC server.
--
-- @param functionName The function being called.
-- @param typeArray    An array that specifies the types of the function arguments.
-- @param parameters   Arguments that are passed to the server function.
-- @param void         A boolean value that indiciates if the function has a return.
-- @param rpcHost      The RPC service host address.
-- @param rpcPort      The RPC service port number.
-- @return function    A stub function that encapsulate the RPC request in a series of sends and receives primitives.
local function rpcStub(functionName, typeArray, parameters, rpcHost, rpcPort)
  local client = getSocket(rpcHost, rpcPort)

  if client then
    local connected, msg = client:send(functionName .. "\r\n")
  
    for _,v in pairs(parameters) do
      client:send(v .. "\r\n")
    end

    local responseSize = #typeArray - #parameters + 1
    local response     = {}

    local errorCode    = false
    for i = 1, responseSize do
      response[i], err = client:receive('*l')
      -- check if socket was closed, if it was reconnect and remake the call
      if (err == "closed") then
        renewConnection (rpcHost, rpcPort)
        return rpcStub(functionName, typeArray, parameters, rpcHost, rpcPort)
      end
      
      -- check if an error ocurred
      if (string.find(response[1], "___ERRORPC") ~= nil) then
        errorCode = true
      end
    end
    
    if (errorCode) then
      response = {"nil", response[1]}
    end
    
    response = unnormalize(response)

    -- FIXME: type check response
    return unpack(response)
  else
    -- If we cannot connect, we should return an error function (as specified in the spec).
    return nil, errorFunction("Could not connect to " .. rpcHost .. ":" .. rpcPort)
  end
end

--- Generate a server skeleton for a RPC reply.
-- This function generates an anonymous function, skeletons, that unmarshall the
-- client RPC request into a series of parameters that are passed to the servant object.
-- After the call, if the RPC request has out parameters our return value a series of send
-- primitives are made as reply for the RPC client.
--
-- @param servantObject The servant object that has the function implementation.
-- @return function A skeleton function for each idl method.
local function rpcSkel(servantObject)
  local functions = {}
  local currentServant = servantObject

  for methodName, fields in pairs(idl.methods) do
    -- discover the number of params (run once for each function)
    local numParam = 0
    local outParam = 0
    for _, v in pairs(fields.args) do
      if (v.direction == "in") then
        numParam = numParam + 1
      elseif (v.direction == "inout") then 
        numParam = numParam + 1
        outParam = outParam + 1
      else
        outParam = outParam + 1
      end
    end
    
    if ((fields.resulttype ~= "void") or (fields.resulttype == "void" and outParam == 0)) then
      outParam = outParam + 1
    end

    -- with the function info we generate the skeleton
    functions[methodName] =
      function (sck)
        local resultType   = fields.resulttype
        local expectedArgs = fields.args

        -- reading parameters
        local params = {}
        for i = 1, numParam do
          params[i] = sck:receive("*l")
        end
        params = unnormalize(params)
 
        local check       = checkParameters(expectedArgs, params)
        local rpcFunction = currentServant[methodName]
        local reply       = {}
        
        if rpcFunction ~= nil then 
          reply = {rpcFunction(unpack(params))}
          reply = normalize(reply, outParam)
        else
          local host, port = sck:getsockname()
          reply = {errorFunction("Servant " .. host .. ":" .. port .. " does not have " .. methodName)}
          reply = normalize(reply, 1)
        end
        
        for _, v in pairs(reply) do
          sck:send(v .. "\n")
        end
      end
  end
  return functions
end

--- Creates a RPC request proxy with a server. 
-- The function check the validity of the call before redirecting to the stub.
-- The stub apply a series of send and receive primitives to send the RPC request.
-- @param rpcHost The RPC server hostname.
-- @param rpcPort The RPC server port.
-- @return functions A table containing proxy for each of the idl method.
local function rpcProxy(rpcHost, rpcPort)
  local functions = {}
  for methodName, fields in pairs(idl.methods) do
    functions[methodName] =
      function (...)
        local resultType   = fields.resulttype
        local expectedArgs = fields.args
        local params       = {...}
  
        local check = checkParameters(expectedArgs, params)
        if (check) then
          --send a message to server
          params = normalize (params, select("#", ...))
          return rpcStub(methodName, expectedArgs, params, rpcHost, rpcPort)
        else
          errorFunction("Parameters not corrected.")
        end
      end
  end
  return functions
end

--- A utilitary function to remove a element from a table.
-- @param tbl A table.
-- @param value The element to remove.
-- @return tbl A table without the element value.
local function removeElement(tbl, value)
  for k, v in pairs(tbl) do
    if v == value then
      table.remove(tbl, k)
      break
    end
  end
end

--- This functions handles a client RPC request.
-- This function deserialize the client RPC request and identify the correct skeleton to redirect the RPC request.
-- If an error occurs in the skeleton identification, i.e., the  read method return an err with the string "close"
-- then the client socket is removed from the available sockets.
-- @param sck The client socket.
local function handleClient(sck)
  -- discover the function name
  local functionName, err = sck:receive("*l")
  
  -- only continue if the client is not closed
  if (err ~= "closed") then
    -- retrieve network information to identify rpc request
    local host, port = sck:getsockname()
    
    -- retrieve skeleton RPC.using the port
    rpcServants[port][functionName](sck)
  else
    -- client closed the connection
    sck:close()
    removeElement(waitSockets, sck)
    removeElement(clientSockets, sck)
    
    if clientcount > 0 then
      clientcount = clientcount - 1
    end
  end
end

--- Adds a client to the pool queue.
-- @param clientsck A TCP client socket.
local function addClient(clientsck)
  clientcount = clientcount + 1
  
  if clientcount > RPC.poolsize then -- remove a client
    local removedClient = table.remove(clientSockets, 1)
    removeElement(waitSockets, removedClient)
    removedClient:close()
    
    clientcount = clientcount - 1
  end
  table.insert(waitSockets,   clientsck)
  table.insert(clientSockets, clientsck)
end

-------------------------------------------------------------------------------
-- public interface
-------------------------------------------------------------------------------

--- Create a RPC servant following the idl methods and redirecting calls to the servant object.
-- This function creates a RPC servant that will listen to RPC requests.
-- Note, that idl is a string containing a file in the current directory.
-- @param idl The name of the IDL file that the servant implement.
-- @param servantObject The servant object that implements the RPC IDL.
-- @return host The servant listening hostname.
-- @return port The servant listening port.
function RPC.createServant(idl, servantObject)
  if (servantObject ~= nil) then
    server = assert(socket.bind ("*", 0))
    server:setoption("tcp-nodelay", true)
    
    sckts[#sckts + 1] = server

    -- associate the servant object with the socket (dummy)
    local host, port = server:getsockname()

    -- generate skeletons
    dofile(idl)

    rpcServants[port] = rpcSkel(servantObject)

    return host, port
  end
end


--- Wait for RPC requests.
-- This function listen forever (while true) for RPC requests to the servants registered in this server.
function RPC.waitIncoming()
  print("Server Starting")
  print("Using Pool", RPC.usingpool)
  print("Pool Size", RPC.poolsize)
  if (#sckts >= 1) then
    for _, sck in ipairs(sckts) do
      table.insert(waitSockets, sck)
    end

    while true do
      local canRead = socket.select(waitSockets, nil)

      -- select could return more than one socket
      for _, sck in ipairs(canRead) do
        -- We have to check if 'sck' is a server or client socket.
        -- If it is a server socket, we must accept the connection and insert a new created client
        -- socket in 'waitSockets' table in order to be able to answer client's requests.
        -- If it's a client socket, we have to handle the request.

        -- case is a server socket, we must accept the connection
        local isServerSocket = false
        for _, serverSocket in ipairs(sckts) do
          if (sck == serverSocket) then
            local clientSocket = sck:accept()
            if (clientSocket ~= nil) then
              clientSocket:setoption("tcp-nodelay", true)

              if not RPC.usingpool then
                table.insert(waitSockets, clientSocket)
              else
                addClient(clientSocket)
              end
              isServerSocket = true
              break
            end --if (clientSocket...
          end --if (sck...
        end --for _, serverSocket...

        -- case is a client socket, we handle the request
        if not isServerSocket then -- handle client request
          handleClient(sck)
          if not RPC.usingpool then
            sck:close()
          end
        end --if not...
      end --for _, sck...
    end --while 1...
  end --if (#sckts...
end

--- Creates a RPC proxy for a client.
-- This functions creates a RPC proxy following a given IDL to a servant located in ip and port.
-- @param ip RPC servant ip.
-- @param port RPC servant port.
-- @param idl RPC idl.
-- @return tbl A RPC proxy for each function of the IDL.
function RPC.createProxy(ip, port, idl)
  dofile(idl)

  local  tbl = rpcProxy(ip, port)
  return tbl
end

-- return the module
return RPC
